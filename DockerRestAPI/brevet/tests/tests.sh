#! /bin/bash
# 
# Test the trivial web server.  
# Usage:  tests.sh http://the.IP.address:port/path
# Example: tests.sh http://127.0.0.1:5000/sample.html
#
URLbase=$1

# Test cases for the body 
# 

function expect_post_body() {
    # Args
    path=$1
    data=$2
    # data='{"test":123}'
    expect=$3
    curl --silent -i -X POST --data ${data} ${URLbase}/${path} >/tmp/,$$
    if grep -q ${expect} /tmp/,$$ ; then 
	echo "Pass --  found ${expect} in ${path}"
    else
        echo "*** FAIL *** expecting ${expect} in  ${URLbase}/${path}"
        cat /tmp/,$$
    fi
}

function expect_get_body() {
    # Args
    path=$1
    expect=$2
    curl --silent ${URLbase}/${path} >/tmp/,$$
    if grep -q ${expect} /tmp/,$$ ; then 
	echo "Pass --  found ${expect} in ${path}"
    else
        echo "*** FAIL *** expecting ${expect} in  ${URLbase}/${path}"
    fi
}

expect_get_body display  "\[\]"
expect_post_body submit "brevet_dist_km=200" "failed"
expect_post_body submit "brevet_dist_km=200&begin_time=2017-01-01T00%3A00%3A00%2B00%3A00&control_times=%5B%7B%22miles%22%3A%2262.1%22%2C%22km%22%3A%22100%22%2C%22location%22%3A%22%22%2C%22open%22%3A%222017-01-01T02%3A56%3A00%2B00%3A00%22%2C%22close%22%3A%222017-01-01T06%3A40%3A00%2B00%3A00%22%7D%5D" "success"
expect_get_body display  "brevet_dist_km"
