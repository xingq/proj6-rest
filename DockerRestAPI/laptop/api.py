# Laptop Service
import os
import arrow
import flask
from flask import Flask
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient
from flask_cors import CORS

# Instantiate the app
app = Flask(__name__)
api = Api(app)
cors = CORS(app)
client = MongoClient("db", 27017)
db = client.brevets

parser = reqparse.RequestParser()
parser.add_argument('top', type=int)

class BrevetList(Resource):
    def get(self, format='json'):
        _items = db.brevets.find()
        control_times = []
        for item in _items:
            for control_time in item['control_times']:
                control_times.append({
                    'open': control_time['open'],
                    'close': control_time['close']
                })
        app.logger.debug(control_times)

        if format == 'csv':
            csv_rows = []
            for control_time in control_times:
                csv_rows.append("{},{}".format(control_time['open'], control_time['close']))
            csv = "\n".join(csv_rows)
            response = flask.make_response(csv)
            response.headers['content-type'] = 'application/octet-stream'
            return response
        else:
            return control_times
class BrevetOpenList(Resource):
    def get(self, format='json'):
        _items = db.brevets.find()
        control_times = []
        for item in _items:
            for control_time in item['control_times']:
                control_times.append(control_time['open'])
        app.logger.debug(control_times)

        args = parser.parse_args()
        if 'top' in args and args['top'] != None and args['top'] > 0:
            control_times = sorted(control_times, key=lambda x: arrow.get(x).timestamp)
            control_times = control_times[:args['top']]

        if format == 'csv':
            csv_rows = []
            csv = "\n".join(control_times)
            response = flask.make_response(csv)
            response.headers['content-type'] = 'application/octet-stream'
            return response
        else:
            return control_times

class BrevetCloseList(Resource):
    def get(self, format='json'):
        _items = db.brevets.find()
        control_times = []
        for item in _items:
            for control_time in item['control_times']:
                control_times.append(control_time['close'])
        app.logger.debug(control_times)

        args = parser.parse_args()
        if 'top' in args and args['top'] != None and args['top'] > 0:
            control_times = sorted(control_times, key=lambda x: arrow.get(x).timestamp)
            control_times = control_times[:args['top']]

        if format == 'csv':
            csv_rows = []
            csv = "\n".join(control_times)
            response = flask.make_response(csv)
            response.headers['content-type'] = 'application/octet-stream'
            return response
        else:
            return control_times

# Create routes
# Another way, without decorators
api.add_resource(BrevetList, '/listAll/<string:format>', '/listAll')
api.add_resource(BrevetOpenList, '/listOpenOnly/<string:format>', '/listOpenOnly')
api.add_resource(BrevetCloseList, '/listCloseOnly/<string:format>', '/listCloseOnly')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
