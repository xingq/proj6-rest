
import unittest
import json
from api import app
from pymongo import MongoClient

client = MongoClient("db", 27017)
db = client.brevets

class TestLogin(unittest.TestCase):
    def setUp(self):

        self.app = app
        self.client = app.test_client()
        item_doc = {
            'brevet_dist_km': 200,
            'begin_time': '2020-11-18T00:00:00+00:00',
            'control_times': [
                {"miles":"74.6","km":"120","location":"","open":"2020-11-18T03:32:00+00:00","close":"2020-11-18T08:00:00+00:00"},
                {"miles":"93.2","km":"150","location":"","open":"2020-11-18T04:25:00+00:00","close":"2020-11-18T10:00:00+00:00"},
                {"miles":"111.8","km":"180","location":"","open":"2020-11-18T05:18:00+00:00","close":"2020-11-18T12:00:00+00:00"},
                {"miles":"62.1","km":"100","location":"","open":"2020-11-18T02:56:00+00:00","close":"2020-11-18T06:40:00+00:00"},
                {"miles":"124.3","km":"200","location":"","open":"2020-11-18T05:53:00+00:00","close":"2020-11-18T13:30:00+00:00"}
            ]
        }
        db.brevets.remove()
        db.brevets.insert_one(item_doc)


    def tearDown(self):
        db.brevets.remove()


    def test_list_all_json(self):
        response = self.client.get("/listAll")
        resp_dict = json.loads(response.data)
        self.assertEqual(5, len(resp_dict))
        self.assertIn("open", resp_dict[0])
        self.assertIn("close", resp_dict[0])

        response = self.client.get("/listAll/json")
        resp_dict = json.loads(response.data)
        self.assertEqual(5, len(resp_dict))
        self.assertIn("open", resp_dict[0])
        self.assertIn("close", resp_dict[0])
    
    def test_list_all_csv(self):
        response = self.client.get("/listAll/csv")
        rows = str(response.data, "utf-8").split("\n")
        self.assertEqual(5, len(rows))
        self.assertEqual(2, len(rows[0].split(",")))
    
    def test_list_open_json(self):
        response = self.client.get("/listOpenOnly")
        resp_dict = json.loads(response.data)
        self.assertEqual(5, len(resp_dict))

        response = self.client.get("/listOpenOnly/json")
        resp_dict = json.loads(response.data)
        self.assertEqual(5, len(resp_dict))

    def test_list_open_csv(self):
        response = self.client.get("/listOpenOnly/csv")
        rows = str(response.data, "utf-8").split("\n")
        self.assertEqual(5, len(rows))
        self.assertEqual(1, len(rows[0].split(",")))

    def test_list_open_top(self):
        response = self.client.get("/listOpenOnly?top=3")
        resp_dict = json.loads(response.data)
        self.assertEqual(3, len(resp_dict))
        self.assertEqual("2020-11-18T02:56:00+00:00", resp_dict[0])

    def test_list_close_json(self):
        response = self.client.get("/listCloseOnly")
        resp_dict = json.loads(response.data)
        self.assertEqual(5, len(resp_dict))

        response = self.client.get("/listCloseOnly/json")
        resp_dict = json.loads(response.data)
        self.assertEqual(5, len(resp_dict))

    def test_list_close_csv(self):
        response = self.client.get("/listCloseOnly/csv")
        rows = str(response.data, "utf-8").split("\n")
        self.assertEqual(5, len(rows))
        self.assertEqual(1, len(rows[0].split(",")))

    def test_list_close_top(self):
        response = self.client.get("/listCloseOnly?top=3")
        resp_dict = json.loads(response.data)
        self.assertEqual(3, len(resp_dict))
        self.assertEqual("2020-11-18T06:40:00+00:00", resp_dict[0])
