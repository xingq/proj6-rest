# README

## Author: Xing Qian, xingq@uoregon.edu

This is a solution of the assignment "project6" project for CIS 322

You can view the project description: https://bitbucket.org/UOCIS322/proj6-rest/src/master/

## Introduction of my solution

The assignment asked for designing restful apis of brevets control time system, and I have reached this goal.

## Run the project

### Start Services

```
# go to the `DockerRestAPI` folder, and run the following command.
# at first, we need to build the image and run the image
cd DockerRestAPI

# start the server
docker-compose up
```

### Run Tests

After start the services, you need to go to the laptop container, and run the unittest.
```
python -m unittest tests/test_laptop.py
```

### Services and Containers

- brevet: This service is in brevet container.
- consumer: This service is in website container.
- rest api: This service is in laptop-service container.

### View Control Time Management View
- http://localhost:5002/: Open this view you could see the brevets page like `project5`, you need add some control times in this page, and then test the restful apis.

### View Consumer View

- http://localhost:5000: Open this view you could see the consumer system, select which data type (`json`/`csv`) you need, and input the top number, and then click buttons to get the open or close times.
- If you choose json, the data would display on the page.
- If you choose csv, you would need download csv data and view it.

### Pay Attention

- The consumer view port is `5000`, the apis port is `5001`, the brevet page and brevet apis(CURD of brevet times) port is `5002`

## API Instruction

### Routes

I have build apis as the requirements, there are the routes examples:

- http://localhost:5001/listAll
- http://localhost:5001/listAll/csv
- http://localhost:5001/listAll/json
- http://localhost:5001/listOpenOnly
- http://localhost:5001/listOpenOnly/csv
- http://localhost:5001/listOpenOnly/json
- http://localhost:5001/listOpenOnly/json?top=3
- http://localhost:5001/listOpenOnly/csv?top=3
- http://localhost:5001/listCloseOnly
- http://localhost:5001/listCloseOnly/csv
- http://localhost:5001/listCloseOnly/json
- http://localhost:5001/listCloseOnly/json?top=3
- http://localhost:5001/listCloseOnly/csv?top=3
  
### Response

**Route**: http://localhost:5001/listAll

This api would return open and close time in the mongo, default format is `json`, looks like:
```
[
    {
        "close": "2020-11-23T06:40:00+00:00",
        "open": "2020-11-23T02:56:00+00:00"
    },
    ...
    {
        "close": "2020-11-23T10:00:00+00:00",
        "open": "2020-11-23T04:25:00+00:00"
    },
]
```

If you choose return `csv` format, the data would looks like:
```
2020-11-23T02:56:00+00:00,2020-11-23T06:40:00+00:00
...
2020-11-23T02:56:00+00:00,2020-11-23T06:40:00+00:00
```
It contains tow columns, first column is open time, second column is close time.

**Route**: http://localhost:5001/listOpenOnly or http://localhost:5001/listCloseOnly

This api would return open or close time in the mongo, default format is `json`, looks like:
```
[
    "2020-11-18T02:56:00+00:00",
    "2020-11-18T03:32:00+00:00",
    "2020-11-18T04:25:00+00:00",
    "2020-11-18T05:18:00+00:00"
]
```

If you choose return `csv` format, the data would looks like:
```
2020-11-23T02:56:00+00:00
...
2020-11-23T02:56:00+00:00
```
It contains one column denotes open or close time.

